﻿Imports System.Runtime.InteropServices

Namespace IO.Input

    Public Class Input
#Region "register raw devices"
        ''' <summary>Function to register a raw input device.</summary>
        ''' <param name="pRawInputDevices">Array of raw input devices.</param>
        ''' <param name="uiNumDevices">Number of devices.</param>
        ''' <param name="cbSize">Size of the RAWINPUTDEVICE structure.</param>
        ''' <returns>True if successful, False if not.</returns>
        <DllImport("user32.dll")>
        Public Shared Function RegisterRawInputDevices(<MarshalAs(UnmanagedType.LPArray, SizeParamIndex:=0)> ByVal pRawInputDevices As RAWINPUTDEVICE(), ByVal uiNumDevices As Integer, ByVal cbSize As Integer) As Boolean
        End Function

        ''' <summary>HID usage page flags.</summary>

        Public Enum HIDUsagePage As UShort
            ''' <summary>Unknown usage page.</summary>
            Undefined = &H0
            ''' <summary>Generic desktop controls.</summary>
            Generic = &H1
            ''' <summary>Simulation controls.</summary>
            Simulation = &H2
            ''' <summary>Virtual reality controls.</summary>
            VR = &H3
            ''' <summary>Sports controls.</summary>
            Sport = &H4
            ''' <summary>Games controls.</summary>
            Game = &H5
            ''' <summary>Keyboard controls.</summary>
            Keyboard = &H7
            ''' <summary>LED controls.</summary>
            LED = &H8
            ''' <summary>Button.</summary>
            Button = &H9
            ''' <summary>Ordinal.</summary>
            Ordinal = &HA
            ''' <summary>Telephony.</summary>
            Telephony = &HB
            ''' <summary>Consumer.</summary>
            Consumer = &HC
            ''' <summary>Digitizer.</summary>
            Digitizer = &HD
            ''' <summary>Physical interface device.</summary>
            PID = &HF
            ''' <summary>Unicode.</summary>
            Unicode = &H10
            ''' <summary>Alphanumeric display.</summary>
            AlphaNumeric = &H14
            ''' <summary>Medical instruments.</summary>
            Medical = &H40
            ''' <summary>Monitor page 0.</summary>
            MonitorPage0 = &H80
            ''' <summary>Monitor page 1.</summary>
            MonitorPage1 = &H81
            ''' <summary>Monitor page 2.</summary>
            MonitorPage2 = &H82
            ''' <summary>Monitor page 3.</summary>
            MonitorPage3 = &H83
            ''' <summary>Power page 0.</summary>
            PowerPage0 = &H84
            ''' <summary>Power page 1.</summary>
            PowerPage1 = &H85
            ''' <summary>Power page 2.</summary>
            PowerPage2 = &H86
            ''' <summary>Power page 3.</summary>
            PowerPage3 = &H87
            ''' <summary>Bar code scanner.</summary>
            BarCode = &H8C
            ''' <summary>Scale page.</summary>
            Scale = &H8D
            ''' <summary>Magnetic strip reading devices.</summary>
            MSR = &H8E
        End Enum
        ''' <summary>
        ''' Enumeration containing the HID usage values.
        ''' </summary>
        Public Enum HIDUsage As UShort
            ''' <summary></summary>
            Pointer = &H1
            ''' <summary></summary>
            Mouse = &H2
            ''' <summary></summary>
            Joystick = &H4
            ''' <summary></summary>
            Gamepad = &H5
            ''' <summary></summary>
            Keyboard = &H6
            ''' <summary></summary>
            Keypad = &H7
            ''' <summary></summary>
            SystemControl = &H80
            ''' <summary></summary>
            X = &H30
            ''' <summary></summary>
            Y = &H31
            ''' <summary></summary>
            Z = &H32
            ''' <summary></summary>
            RelativeX = &H33
            ''' <summary></summary>    
            RelativeY = &H34
            ''' <summary></summary>
            RelativeZ = &H35
            ''' <summary></summary>
            Slider = &H36
            ''' <summary></summary>
            Dial = &H37
            ''' <summary></summary>
            Wheel = &H38
            ''' <summary></summary>
            HatSwitch = &H39
            ''' <summary></summary>
            CountedBuffer = &H3A
            ''' <summary></summary>
            ByteCount = &H3B
            ''' <summary></summary>
            MotionWakeup = &H3C
            ''' <summary></summary>
            VX = &H40
            ''' <summary></summary>
            VY = &H41
            ''' <summary></summary>
            VZ = &H42
            ''' <summary></summary>
            VBRX = &H43
            ''' <summary></summary>
            VBRY = &H44
            ''' <summary></summary>
            VBRZ = &H45
            ''' <summary></summary>
            VNO = &H46
            ''' <summary></summary>
            SystemControlPower = &H81
            ''' <summary></summary>
            SystemControlSleep = &H82
            ''' <summary></summary>
            SystemControlWake = &H83
            ''' <summary></summary>
            SystemControlContextMenu = &H84
            ''' <summary></summary>
            SystemControlMainMenu = &H85
            ''' <summary></summary>
            SystemControlApplicationMenu = &H86
            ''' <summary></summary>
            SystemControlHelpMenu = &H87
            ''' <summary></summary>
            SystemControlMenuExit = &H88
            ''' <summary></summary>
            SystemControlMenuSelect = &H89
            ''' <summary></summary>
            SystemControlMenuRight = &H8A
            ''' <summary></summary>
            SystemControlMenuLeft = &H8B
            ''' <summary></summary>
            SystemControlMenuUp = &H8C
            ''' <summary></summary>
            SystemControlMenuDown = &H8D
            ''' <summary></summary>
            KeyboardNoEvent = &H0
            ''' <summary></summary>
            KeyboardRollover = &H1
            ''' <summary></summary>
            KeyboardPostFail = &H2
            ''' <summary></summary>
            KeyboardUndefined = &H3
            ''' <summary></summary>
            KeyboardaA = &H4
            ''' <summary></summary>
            KeyboardzZ = &H1D
            ''' <summary></summary>
            Keyboard1 = &H1E
            ''' <summary></summary>
            Keyboard0 = &H27
            ''' <summary></summary>
            KeyboardLeftControl = &HE0
            ''' <summary></summary>
            KeyboardLeftShift = &HE1
            ''' <summary></summary>
            KeyboardLeftALT = &HE2
            ''' <summary></summary>
            KeyboardLeftGUI = &HE3
            ''' <summary></summary>
            KeyboardRightControl = &HE4
            ''' <summary></summary>
            KeyboardRightShift = &HE5
            ''' <summary></summary>
            KeyboardRightALT = &HE6
            ''' <summary></summary>
            KeyboardRightGUI = &HE7
            ''' <summary></summary>
            KeyboardScrollLock = &H47
            ''' <summary></summary>
            KeyboardNumLock = &H53
            ''' <summary></summary>
            KeyboardCapsLock = &H39
            ''' <summary></summary>
            KeyboardF1 = &H3A
            ''' <summary></summary>
            KeyboardF12 = &H45
            ''' <summary></summary>
            KeyboardReturn = &H28
            ''' <summary></summary>
            KeyboardEscape = &H29
            ''' <summary></summary>
            KeyboardDelete = &H2A
            ''' <summary></summary>
            KeyboardPrintScreen = &H46
            ''' <summary></summary>
            LEDNumLock = &H1
            ''' <summary></summary>
            LEDCapsLock = &H2
            ''' <summary></summary>
            LEDScrollLock = &H3
            ''' <summary></summary>
            LEDCompose = &H4
            ''' <summary></summary>
            LEDKana = &H5
            ''' <summary></summary>
            LEDPower = &H6
            ''' <summary></summary>
            LEDShift = &H7
            ''' <summary></summary>
            LEDDoNotDisturb = &H8
            ''' <summary></summary>
            LEDMute = &H9
            ''' <summary></summary>
            LEDToneEnable = &HA
            ''' <summary></summary>
            LEDHighCutFilter = &HB
            ''' <summary></summary>
            LEDLowCutFilter = &HC
            ''' <summary></summary>
            LEDEqualizerEnable = &HD
            ''' <summary></summary>
            LEDSoundFieldOn = &HE
            ''' <summary></summary>
            LEDSurroundFieldOn = &HF
            ''' <summary></summary>
            LEDRepeat = &H10
            ''' <summary></summary>
            LEDStereo = &H11
            ''' <summary></summary>
            LEDSamplingRateDirect = &H12
            ''' <summary></summary>
            LEDSpinning = &H13
            ''' <summary></summary>
            LEDCAV = &H14
            ''' <summary></summary>
            LEDCLV = &H15
            ''' <summary></summary>
            LEDRecordingFormatDet = &H16
            ''' <summary></summary>
            LEDOffHook = &H17
            ''' <summary></summary>
            LEDRing = &H18
            ''' <summary></summary>
            LEDMessageWaiting = &H19
            ''' <summary></summary>
            LEDDataMode = &H1A
            ''' <summary></summary>
            LEDBatteryOperation = &H1B
            ''' <summary></summary>
            LEDBatteryOK = &H1C
            ''' <summary></summary>
            LEDBatteryLow = &H1D
            ''' <summary></summary>
            LEDSpeaker = &H1E
            ''' <summary></summary>
            LEDHeadset = &H1F
            ''' <summary></summary>
            LEDHold = &H20
            ''' <summary></summary>
            LEDMicrophone = &H21
            ''' <summary></summary>
            LEDCoverage = &H22
            ''' <summary></summary>
            LEDNightMode = &H23
            ''' <summary></summary>
            LEDSendCalls = &H24
            ''' <summary></summary>
            LEDCallPickup = &H25
            ''' <summary></summary>
            LEDConference = &H26
            ''' <summary></summary>
            LEDStandBy = &H27
            ''' <summary></summary>
            LEDCameraOn = &H28
            ''' <summary></summary>
            LEDCameraOff = &H29
            ''' <summary></summary>    
            LEDOnLine = &H2A
            ''' <summary></summary>
            LEDOffLine = &H2B
            ''' <summary></summary>
            LEDBusy = &H2C
            ''' <summary></summary>
            LEDReady = &H2D
            ''' <summary></summary>
            LEDPaperOut = &H2E
            ''' <summary></summary>
            LEDPaperJam = &H2F
            ''' <summary></summary>
            LEDRemote = &H30
            ''' <summary></summary>
            LEDForward = &H31
            ''' <summary></summary>
            LEDReverse = &H32
            ''' <summary></summary>
            LEDStop = &H33
            ''' <summary></summary>
            LEDRewind = &H34
            ''' <summary></summary>
            LEDFastForward = &H35
            ''' <summary></summary>
            LEDPlay = &H36
            ''' <summary></summary>
            LEDPause = &H37
            ''' <summary></summary>
            LEDRecord = &H38
            ''' <summary></summary>
            LEDError = &H39
            ''' <summary></summary>
            LEDSelectedIndicator = &H3A
            ''' <summary></summary>
            LEDInUseIndicator = &H3B
            ''' <summary></summary>
            LEDMultiModeIndicator = &H3C
            ''' <summary></summary>
            LEDIndicatorOn = &H3D
            ''' <summary></summary>
            LEDIndicatorFlash = &H3E
            ''' <summary></summary>
            LEDIndicatorSlowBlink = &H3F
            ''' <summary></summary>
            LEDIndicatorFastBlink = &H40
            ''' <summary></summary>
            LEDIndicatorOff = &H41
            ''' <summary></summary>
            LEDFlashOnTime = &H42
            ''' <summary></summary>
            LEDSlowBlinkOnTime = &H43
            ''' <summary></summary>
            LEDSlowBlinkOffTime = &H44
            ''' <summary></summary>
            LEDFastBlinkOnTime = &H45
            ''' <summary></summary>
            LEDFastBlinkOffTime = &H46
            ''' <summary></summary>
            LEDIndicatorColor = &H47
            ''' <summary></summary>
            LEDRed = &H48
            ''' <summary></summary>
            LEDGreen = &H49
            ''' <summary></summary>
            LEDAmber = &H4A
            ''' <summary></summary>
            LEDGenericIndicator = &H3B
            ''' <summary></summary>
            TelephonyPhone = &H1
            ''' <summary></summary>
            TelephonyAnsweringMachine = &H2
            ''' <summary></summary>
            TelephonyMessageControls = &H3
            ''' <summary></summary>
            TelephonyHandset = &H4
            ''' <summary></summary>
            TelephonyHeadset = &H5
            ''' <summary></summary>
            TelephonyKeypad = &H6
            ''' <summary></summary>
            TelephonyProgrammableButton = &H7
            ''' <summary></summary>
            SimulationRudder = &HBA
            ''' <summary></summary>
            SimulationThrottle = &HBB
        End Enum
        <Flags()>
        Public Enum RawInputDeviceFlags
            ''' <summary>No flags.</summary>
            None = 0
            ''' <summary>If set, this removes the top level collection from the inclusion list. This tells the operating system to stop reading from a device which matches the top level collection.</summary>
            Remove = &H1
            ''' <summary>If set, this specifies the top level collections to exclude when reading a complete usage page. This flag only affects a TLC whose usage page is already specified with PageOnly.</summary>
            Exclude = &H10
            ''' <summary>If set, this specifies all devices whose top level collection is from the specified usUsagePage. Note that Usage must be zero. To exclude a particular top level collection, use Exclude.</summary>
            PageOnly = &H20
            ''' <summary>If set, this prevents any devices specified by UsagePage or Usage from generating legacy messages. This is only for the mouse and keyboard.</summary>
            NoLegacy = &H30
            ''' <summary>If set, this enables the caller to receive the input even when the caller is not in the foreground. Note that WindowHandle must be specified.</summary>
            InputSink = &H100
            ''' <summary>If set, the mouse button click does not activate the other window.</summary>
            CaptureMouse = &H200
            ''' <summary>If set, the application-defined keyboard device hotkeys are not handled. However, the system hotkeys; for example, ALT+TAB and CTRL+ALT+DEL, are still handled. By default, all keyboard hotkeys are handled. NoHotKeys can be specified even if NoLegacy is not specified and WindowHandle is NULL.</summary>
            NoHotKeys = &H200
            ''' <summary>If set, application keys are handled.  NoLegacy must be specified.  Keyboard only.</summary>
            AppKeys = &H400
        End Enum
        ''' <summary>Value type for raw input devices.</summary>
        <StructLayout(LayoutKind.Sequential)>
        Public Structure RAWINPUTDEVICE
            ''' <summary>Top level collection Usage page for the raw input device.</summary>
            Public UsagePage As HIDUsagePage
            ''' <summary>Top level collection Usage for the raw input device. </summary>
            Public Usage As HIDUsage
            ''' <summary>Mode flag that specifies how to interpret the information provided by UsagePage and Usage.</summary>
            Public Flags As RawInputDeviceFlags
            ''' <summary>Handle to the target device. If NULL, it follows the keyboard focus.</summary>
            Public WindowHandle As IntPtr
        End Structure
        Public Structure STORAGE_DEVICE_NUMBER
            Friend DeviceType As Integer
            Friend DeviceNumber As Integer
            Friend PartitionNumber As Integer
        End Structure

        Public Enum EFileAccess As System.Int32
            ''
            ''  The following are masks for the predefined standard access types
            ''

            DELETE = &H10000
            READ_CONTROL = &H20000
            WRITE_DAC = &H40000
            WRITE_OWNER = &H80000
            SYNCHRONIZE = &H100000

            STANDARD_RIGHTS_REQUIRED = &HF0000
            STANDARD_RIGHTS_READ = READ_CONTROL
            STANDARD_RIGHTS_WRITE = READ_CONTROL
            STANDARD_RIGHTS_EXECUTE = READ_CONTROL
            STANDARD_RIGHTS_ALL = &H1F0000
            SPECIFIC_RIGHTS_ALL = &HFFFF

            ''
            '' AccessSystemAcl access type
            ''

            ACCESS_SYSTEM_SECURITY = &H1000000

            ''
            '' MaximumAllowed access type
            ''

            MAXIMUM_ALLOWED = &H2000000

            ''
            ''  These are the generic rights.
            ''

            GENERIC_READ = &H80000000
            GENERIC_WRITE = &H40000000
            GENERIC_EXECUTE = &H20000000
            GENERIC_ALL = &H10000000
        End Enum

        Public Enum EFileShare
            FILE_SHARE_NONE = &H0
            FILE_SHARE_READ = &H1
            FILE_SHARE_WRITE = &H2
            FILE_SHARE_DELETE = &H4
        End Enum

        Public Enum ECreationDisposition
            ''' <summary>
            ''' Creates a new file, only if it does not already exist.
            ''' If the specified file exists, the function fails and the last-error code is set to ERROR_FILE_EXISTS (80).
            ''' If the specified file does not exist and is a valid path to a writable location, a new file is created.
            ''' </summary>
            CREATE_NEW = 1

            ''' <summary>
            ''' Creates a new file, always.
            ''' If the specified file exists and is writable, the function overwrites the file, the function succeeds, and last-error code is set to ERROR_ALREADY_EXISTS (183).
            ''' If the specified file does not exist and is a valid path, a new file is created, the function succeeds, and the last-error code is set to zero.
            ''' For more information, see the Remarks section of this topic.
            ''' </summary>
            CREATE_ALWAYS = 2

            ''' <summary>
            ''' Opens a file or device, only if it exists.
            ''' If the specified file or device does not exist, the function fails and the last-error code is set to ERROR_FILE_NOT_FOUND (2).
            ''' For more information about devices, see the Remarks section.
            ''' </summary>
            OPEN_EXISTING = 3

            ''' <summary>
            ''' Opens a file, always.
            ''' If the specified file exists, the function succeeds and the last-error code is set to ERROR_ALREADY_EXISTS (183).
            ''' If the specified file does not exist and is a valid path to a writable location, the function creates a file and the last-error code is set to zero.
            ''' </summary>
            OPEN_ALWAYS = 4

            ''' <summary>
            ''' Opens a file and truncates it so that its size is zero bytes, only if it exists.
            ''' If the specified file does not exist, the function fails and the last-error code is set to ERROR_FILE_NOT_FOUND (2).
            ''' The calling process must open the file with the GENERIC_WRITE bit set as part of the dwDesiredAccess parameter.
            ''' </summary>
            TRUNCATE_EXISTING = 5
        End Enum

        Public Enum EFileAttributes
            FILE_ATTRIBUTE_READONLY = &H1
            FILE_ATTRIBUTE_HIDDEN = &H2
            FILE_ATTRIBUTE_SYSTEM = &H4
            FILE_ATTRIBUTE_DIRECTORY = &H10
            FILE_ATTRIBUTE_ARCHIVE = &H20
            FILE_ATTRIBUTE_DEVICE = &H40
            FILE_ATTRIBUTE_NORMAL = &H80
            FILE_ATTRIBUTE_TEMPORARY = &H100
            FILE_ATTRIBUTE_SPARSE_FILE = &H200
            FILE_ATTRIBUTE_REPARSE_POINT = &H400
            FILE_ATTRIBUTE_COMPRESSED = &H800
            FILE_ATTRIBUTE_OFFLINE = &H1000
            FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = &H2000
            FILE_ATTRIBUTE_ENCRYPTED = &H4000
            FILE_ATTRIBUTE_VIRTUAL = &H10000

            'This parameter can also contain combinations of flags (FILE_FLAG_*)
            FILE_FLAG_BACKUP_SEMANTICS = &H2000000
            FILE_FLAG_DELETE_ON_CLOSE = &H4000000
            FILE_FLAG_NO_BUFFERING = &H20000000
            FILE_FLAG_OPEN_NO_RECALL = &H100000
            FILE_FLAG_OPEN_REPARSE_POINT = &H200000
            FILE_FLAG_OVERLAPPED = &H40000000
            FILE_FLAG_POSIX_SEMANTICS = &H1000000
            FILE_FLAG_RANDOM_ACCESS = &H10000000
            FILE_FLAG_SEQUENTIAL_SCAN = &H8000000
            FILE_FLAG_WRITE_THROUGH = &H80000000
        End Enum
		<DllImport("kernel32.dll", SetLastError:=True, CharSet:=System.Runtime.InteropServices.CharSet.Auto)>
		Public Shared Function CreateFile(ByVal lpFileName As String,
	ByVal dwDesiredAccess As EFileAccess,
	ByVal dwShareMode As EFileShare,
	ByVal lpSecurityAttributes As IntPtr,
	ByVal dwCreationDisposition As ECreationDisposition,
	ByVal dwFlagsAndAttributes As EFileAttributes,
	ByVal hTemplateFile As IntPtr) As Microsoft.Win32.SafeHandles.SafeFileHandle
		End Function
		Public Enum RawInputDeviceType As UInt32
			MOUSE = 0
			KEYBOARD = 1
			HID = 2
		End Enum
		<StructLayout(LayoutKind.Sequential)>
		Public Structure RAWINPUTDEVICELIST
			Public hDevice As IntPtr
			Public dwType As RawInputDeviceType
		End Structure

		<DllImport("user32.dll", CharSet:=CharSet.Auto,
	EntryPoint:="GetRawInputDeviceList", SetLastError:=True)>
		Public Shared Function GetRawInputDeviceList(ByVal pRawInputDeviceList As _
			IntPtr, ByRef puiNumDevices As Int32, ByVal cbSize As Int32) As Int32
		End Function
#End Region
		Public Property Keyboard As Keyboard
        Public Property Mouse As Mouse
        Public Property Devices As List(Of InputDevice)

		Public Function GetAllRawInputDevices() As List(Of InputDevice)
			Dim ret As New List(Of InputDevice)

			Dim structSize As Integer = Marshal.SizeOf(GetType(RAWINPUTDEVICELIST))
			Dim bufferCount As Integer = 255
			Dim buffer As IntPtr = Marshal.AllocHGlobal(bufferCount * structSize)
			Dim deviceCount As Integer = GetRawInputDeviceList(buffer, bufferCount, structSize)

			For i As Integer = 0 To deviceCount - 1
				Dim device As RAWINPUTDEVICELIST = CType(Marshal.PtrToStructure(New IntPtr((buffer.ToInt32() + (structSize * i))), GetType(RAWINPUTDEVICELIST)), RAWINPUTDEVICELIST)
				Dim tmpDev As InputDevice
				Select Case device.dwType
					Case RawInputDeviceType.HID
						tmpDev = New InputDevice(InputDeviceType.HID)
					Case RawInputDeviceType.KEYBOARD
						tmpDev = New InputDevice(InputDeviceType.Keyboard)
					Case RawInputDeviceType.MOUSE
						tmpDev = New InputDevice(InputDeviceType.Mouse)
					Case Else
						tmpDev = New InputDevice(InputDeviceType.UnknownDevice)
				End Select
			Next
			Return ret
		End Function
	End Class
End Namespace