﻿Imports System.Runtime.InteropServices
Imports System.Timers
Imports SharpDX
Imports WIN = System.Windows.Input

Namespace IO.Input
	Public Class Keyboard
		Inherits InputDevice
#Region "API"
		<DllImport("User32.dll")>
		Public Shared Function ToAscii(ByVal uVirtKey As Int32,
								   ByVal uScanCode As Int32,
								   ByVal lpbKeyState As Byte(),
								   ByVal lpChar As Byte(),
								   ByVal uFlags As Int32) _
								   As Int32
		End Function

		<DllImport("User32.dll")>
		Public Shared Function GetKeyboardState(ByVal pbKeyState As Byte()) _
	  As Int32

		End Function
		<DllImport("user32.dll", CharSet:=CharSet.Auto, EntryPoint:="GetRawInputData", SetLastError:=True)>
		Public Shared Function GetRawInputData(hRawInput As IntPtr,
					uiCommand As RAWINPUTCOMMAND,
					ByRef pData As IntPtr,
					ByRef pcbSize As IntPtr,
					cbSizeHeader As UInt32)
		End Function
		Public Enum RAWINPUTCOMMAND As UInt32
			''' <summary>
			''' Get the raw header
			''' </summary>
			RID_HEADER = &H10000005
			''' <summary>
			''' Get the raw input data
			''' </summary>
			RID_INPUT = &H10000003
		End Enum
		<StructLayout(LayoutKind.Sequential)>
		Public Structure RAWINPUTDEVICELIST
			Public hDevice As IntPtr
			Public dwType As UInt32
		End Structure

		<DllImport("user32.dll", CharSet:=CharSet.Auto,
	EntryPoint:="GetRawInputDeviceList", SetLastError:=True)>
		Public Shared Function GetRawInputDeviceList(ByVal pRawInputDeviceList As _
			IntPtr, ByRef puiNumDevices As Int32, ByVal cbSize As Int32) As Int32
		End Function
		Public Declare Function GetRawInputDeviceInfo Lib "user32.dll" Alias "GetRawInputDeviceInfoW" _
			(ByVal hDevice As IntPtr, ByVal uiCommand As DeviceInfoTypes, ByVal pData As IntPtr,
			 ByRef pcbSize As UInt32) As Int32
		Public Enum DeviceInfoTypes As UInt32
			RIDI_PREPARSEDDATA = &H20000005
			RIDI_DEVICENAME = &H20000007
			RIDI_DEVICEINFO = &H2000000B
		End Enum

		<StructLayout(LayoutKind.Sequential)>
		Public Structure RID_DEVICE_INFO_HID
			<MarshalAs(UnmanagedType.U4)>
			Public dwVendorId As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwProductId As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwVersionNumber As Int32
			<MarshalAs(UnmanagedType.U2)>
			Public usUsagePage As UShort
			<MarshalAs(UnmanagedType.U2)>
			Public usUsage As UShort
		End Structure

		<StructLayout(LayoutKind.Sequential)>
		Public Structure RID_DEVICE_INFO_KEYBOARD
			<MarshalAs(UnmanagedType.U4)>
			Public dwType As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwSubType As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwKeyboardMode As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwNumberOfFunctionKeys As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwNumberOfIndicators As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwNumberOfKeysTotal As Int32
		End Structure

		<StructLayout(LayoutKind.Sequential)>
		Public Structure RID_DEVICE_INFO_MOUSE
			<MarshalAs(UnmanagedType.U4)>
			Public dwId As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwNumberOfButtons As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public dwSampleRate As Int32
			<MarshalAs(UnmanagedType.U4)>
			Public fHasHorizontalWheel As Int32
		End Structure

		<StructLayout(LayoutKind.Explicit)>
		Public Structure RID_DEVICE_INFO
			<FieldOffset(0)>
			Public cbSize As Int32
			<FieldOffset(4)>
			Public dwType As Int32
			<FieldOffset(8)>
			Public mouse As RID_DEVICE_INFO_MOUSE
			<FieldOffset(8)>
			Public keyboard As RID_DEVICE_INFO_KEYBOARD
			<FieldOffset(8)>
			Public hid As RID_DEVICE_INFO_HID
		End Structure

		Public Structure RAWMOUSE

		End Structure
		Public Structure RAWKEYBOARD

		End Structure
		Public Structure RAWHID

		End Structure
		<StructLayout(LayoutKind.Explicit)>
		Public Structure RAWDATASTRUCT
			<FieldOffset(0)>
			Dim mouse As RAWMOUSE
			<FieldOffset(0)>
			Dim keyboard As RAWKEYBOARD
			<FieldOffset(0)>
			Dim hid As RAWHID
		End Structure
		Public Enum RAWHEADERTYPE As UInt32
			''' <summary>
			''' Raw input comes from some device that is not a keyboard or a mouse.
			''' </summary>
			HID = 2
			''' <summary>
			''' Raw input comes from the keyboard.
			''' </summary>
			KEYBOARD = 1
			''' <summary>
			''' Raw input comes from the mouse. 
			''' </summary>
			MOUSE = 0

		End Enum

		<StructLayout(LayoutKind.Sequential)>
		Public Structure RAWHEADERSTRUCT
			Dim Type As RAWHEADERTYPE
			Dim Size As UInt32
			Dim hDevice As IntPtr
			Dim wParam As IntPtr
		End Structure
		<StructLayout(LayoutKind.Sequential)>
		Public Structure RAWINPUT
			Dim header As RAWHEADERSTRUCT
			Dim data As RAWDATASTRUCT
		End Structure

		Private Function GetAsciiCharacter(ByVal uVirtKey As Int32) As Char

			Dim lpKeyState As Byte() = New Byte(255) {}
			GetKeyboardState(lpKeyState)
			Dim lpChar As Byte() = New Byte(1) {}
			If ToAscii(uVirtKey, 0, lpKeyState, lpChar, 0) = 1 Then
				Return Convert.ToChar((lpChar(0)))
			Else
				Return New Char()
			End If
		End Function
		Private Function GetChar() As Int32

		End Function
		Public Enum VK_ENUM As Int32
			'''<summary>
			'''Left mouse button
			'''</summary>
			LBUTTON = &H1
			'''<summary>
			'''Right mouse button
			'''</summary>
			RBUTTON = &H2
			'''<summary>
			'''Control-break processing
			'''</summary>
			CANCEL = &H3
			'''<summary>
			'''Middle mouse button (three-button mouse)
			'''</summary>
			MBUTTON = &H4
			'''<summary>
			'''Windows 2000/XP: X1 mouse button
			'''</summary>
			XBUTTON1 = &H5
			'''<summary>
			'''Windows 2000/XP: X2 mouse button
			'''</summary>
			XBUTTON2 = &H6
			'''<summary>
			'''BACKSPACE key
			'''</summary>
			BACK = &H8
			'''<summary>
			'''TAB key
			'''</summary>
			TAB = &H9
			'''<summary>
			'''CLEAR key
			'''</summary>
			CLEAR = &HC
			'''<summary>
			'''ENTER key
			'''</summary>
			ENTER = &HD
			'''<summary>
			'''SHIFT key
			'''</summary>
			SHIFT = &H10
			'''<summary>
			'''CTRL key
			'''</summary>
			CONTROL = &H11
			'''<summary>
			'''ALT key
			'''</summary>
			MENU = &H12
			'''<summary>
			'''PAUSE key
			'''</summary>
			PAUSE = &H13
			'''<summary>
			'''CAPS LOCK key
			'''</summary>
			CAPITAL = &H14
			'''<summary>
			'''Input Method Editor (IME) Kana mode
			'''</summary>
			KANA = &H15
			'''<summary>
			'''IME Hangul mode
			'''</summary>
			HANGUL = &H15
			'''<summary>
			'''IME Junja mode
			'''</summary>
			JUNJA = &H17
			'''<summary>
			'''IME final mode
			'''</summary>
			FINAL = &H18
			'''<summary>
			'''IME Hanja mode
			'''</summary>
			HANJA = &H19
			'''<summary>
			'''IME Kanji mode
			'''</summary>
			KANJI = &H19
			'''<summary>
			'''ESC key
			'''</summary>
			ESCAPE = &H1B
			'''<summary>
			'''IME convert
			'''</summary>
			CONVERT = &H1C
			'''<summary>
			'''IME nonconvert
			'''</summary>
			NONCONVERT = &H1D
			'''<summary>
			'''IME accept
			'''</summary>
			ACCEPT = &H1E
			'''<summary>
			'''IME mode change request
			'''</summary>
			MODECHANGE = &H1F
			'''<summary>
			'''SPACEBAR
			'''</summary>
			SPACE = &H20
			'''<summary>
			'''PAGE UP key
			'''</summary>
			PAGE_UP = &H21
			'''<summary>
			'''PAGE DOWN key
			'''</summary>
			PAGE_DOWN = &H22
			'''<summary>
			'''END key
			'''</summary>
			END_KEY = &H23
			'''<summary>
			'''HOME key
			'''</summary>
			HOME = &H24
			'''<summary>
			'''LEFT ARROW key
			'''</summary>
			LEFT = &H25
			'''<summary>
			'''UP ARROW key
			'''</summary>
			UP = &H26
			'''<summary>
			'''RIGHT ARROW key
			'''</summary>
			RIGHT = &H27
			'''<summary>
			'''DOWN ARROW key
			'''</summary>
			DOWN = &H28
			'''<summary>
			'''SELECT key
			'''</summary>
			SELECT_KEY = &H29
			'''<summary>
			'''PRINT key
			'''</summary>
			PRINT = &H2A
			'''<summary>
			'''EXECUTE key
			'''</summary>
			EXECUTE = &H2B
			'''<summary>
			'''PRINT SCREEN key
			'''</summary>
			SNAPSHOT = &H2C
			'''<summary>
			'''INS key
			'''</summary>
			INSERT = &H2D
			'''<summary>
			'''DEL key
			'''</summary>
			DELETE = &H2E
			'''<summary>
			'''HELP key
			'''</summary>
			HELP = &H2F
			'''<summary>
			'''0 key
			'''</summary>
			KEY_0 = &H30
			'''<summary>
			'''1 key
			'''</summary>
			KEY_1 = &H31
			'''<summary>
			'''2 key
			'''</summary>
			KEY_2 = &H32
			'''<summary>
			'''3 key
			'''</summary>
			KEY_3 = &H33
			'''<summary>
			'''4 key
			'''</summary>
			KEY_4 = &H34
			'''<summary>
			'''5 key
			'''</summary>
			KEY_5 = &H35
			'''<summary>
			'''6 key
			'''</summary>
			KEY_6 = &H36
			'''<summary>
			'''7 key
			'''</summary>
			KEY_7 = &H37
			'''<summary>
			'''8 key
			'''</summary>
			KEY_8 = &H38
			'''<summary>
			'''9 key
			'''</summary>
			KEY_9 = &H39
			'''<summary>
			'''A key
			'''</summary>
			KEY_A = &H41
			'''<summary>
			'''B key
			'''</summary>
			KEY_B = &H42
			'''<summary>
			'''C key
			'''</summary>
			KEY_C = &H43
			'''<summary>
			'''D key
			'''</summary>
			KEY_D = &H44
			'''<summary>
			'''E key
			'''</summary>
			KEY_E = &H45
			'''<summary>
			'''F key
			'''</summary>
			F = &H46
			'''<summary>
			'''G key
			'''</summary>
			KEY_G = &H47
			'''<summary>
			'''H key
			'''</summary>
			KEY_H = &H48
			'''<summary>
			'''I key
			'''</summary>
			KEY_I = &H49
			'''<summary>
			'''J key
			'''</summary>
			KEY_J = &H4A
			'''<summary>
			'''K key
			'''</summary>
			KEY_K = &H4B
			'''<summary>
			'''L key
			'''</summary>
			KEY_L = &H4C
			'''<summary>
			'''M key
			'''</summary>
			KEY_M = &H4D
			'''<summary>
			'''N key
			'''</summary>
			KEY_N = &H4E
			'''<summary>
			'''O key
			'''</summary>
			KEY_O = &H4F
			'''<summary>
			'''P key
			'''</summary>
			KEY_P = &H50
			'''<summary>
			'''Q key
			'''</summary>
			KEY_Q = &H51
			'''<summary>
			'''R key
			'''</summary>
			KEY_R = &H52
			'''<summary>
			'''S key
			'''</summary>
			KEY_S = &H53
			'''<summary>
			'''T key
			'''</summary>
			KEY_T = &H54
			'''<summary>
			'''U key
			'''</summary>
			KEY_U = &H55
			'''<summary>
			'''V key
			'''</summary>
			KEY_V = &H56
			'''<summary>
			'''W key
			'''</summary>
			KEY_W = &H57
			'''<summary>
			'''X key
			'''</summary>
			KEY_X = &H58
			'''<summary>
			'''Y key
			'''</summary>
			KEY_Y = &H59
			'''<summary>
			'''Z key
			'''</summary>
			KEY_Z = &H5A
			'''<summary>
			'''Left Windows key (Microsoft Natural keyboard)
			'''</summary>
			LWIN = &H5B
			'''<summary>
			'''Right Windows key (Natural keyboard)
			'''</summary>
			RWIN = &H5C
			'''<summary>
			'''Applications key (Natural keyboard)
			'''</summary>
			APPS = &H5D
			'''<summary>
			'''Computer Sleep key
			'''</summary>
			SLEEP = &H5F
			'''<summary>
			'''Numeric keypad 0 key
			'''</summary>
			NUMPAD0 = &H60
			'''<summary>
			'''Numeric keypad 1 key
			'''</summary>
			NUMPAD1 = &H61
			'''<summary>
			'''Numeric keypad 2 key
			'''</summary>
			NUMPAD2 = &H62
			'''<summary>
			'''Numeric keypad 3 key
			'''</summary>
			NUMPAD3 = &H63
			'''<summary>
			'''Numeric keypad 4 key
			'''</summary>
			NUMPAD4 = &H64
			'''<summary>
			'''Numeric keypad 5 key
			'''</summary>
			NUMPAD5 = &H65
			'''<summary>
			'''Numeric keypad 6 key
			'''</summary>
			NUMPAD6 = &H66
			'''<summary>
			'''Numeric keypad 7 key
			'''</summary>
			NUMPAD7 = &H67
			'''<summary>
			'''Numeric keypad 8 key
			'''</summary>
			NUMPAD8 = &H68
			'''<summary>
			'''Numeric keypad 9 key
			'''</summary>
			NUMPAD9 = &H69
			'''<summary>
			'''Multiply key
			'''</summary>
			MULTIPLY = &H6A
			'''<summary>
			'''Add key
			'''</summary>
			ADD = &H6B
			'''<summary>
			'''Separator key
			'''</summary>
			SEPARATOR = &H6C
			'''<summary>
			'''Subtract key
			'''</summary>
			SUBTRACT = &H6D
			'''<summary>
			'''Decimal key
			'''</summary>
			DECIMAL_KEY = &H6E
			'''<summary>
			'''Divide key
			'''</summary>
			DIVIDE = &H6F
			'''<summary>
			'''F1 key
			'''</summary>
			F1 = &H70
			'''<summary>
			'''F2 key
			'''</summary>
			F2 = &H71
			'''<summary>
			'''F3 key
			'''</summary>
			F3 = &H72
			'''<summary>
			'''F4 key
			'''</summary>
			F4 = &H73
			'''<summary>
			'''F5 key
			'''</summary>
			F5 = &H74
			'''<summary>
			'''F6 key
			'''</summary>
			F6 = &H75
			'''<summary>
			'''F7 key
			'''</summary>
			F7 = &H76
			'''<summary>
			'''F8 key
			'''</summary>
			F8 = &H77
			'''<summary>
			'''F9 key
			'''</summary>
			F9 = &H78
			'''<summary>
			'''F10 key
			'''</summary>
			F10 = &H79
			'''<summary>
			'''F11 key
			'''</summary>
			F11 = &H7A
			'''<summary>
			'''F12 key
			'''</summary>
			F12 = &H7B
			'''<summary>
			'''F13 key
			'''</summary>
			F13 = &H7C
			'''<summary>
			'''F14 key
			'''</summary>
			F14 = &H7D
			'''<summary>
			'''F15 key
			'''</summary>
			F15 = &H7E
			'''<summary>
			'''F16 key
			'''</summary>
			F16 = &H7F
			'''<summary>
			'''F17 key  
			'''</summary>
			F17 = &H80
			'''<summary>
			'''F18 key  
			'''</summary>
			F18 = &H81
			'''<summary>
			'''F19 key  
			'''</summary>
			F19 = &H82
			'''<summary>
			'''F20 key  
			'''</summary>
			F20 = &H83
			'''<summary>
			'''F21 key  
			'''</summary>
			F21 = &H84
			'''<summary>
			'''F22 key (PPC only) Key used to lock device.
			'''</summary>
			F22 = &H85
			'''<summary>
			'''F23 key  
			'''</summary>
			F23 = &H86
			'''<summary>
			'''F24 key  
			'''</summary>
			F24 = &H87
			'''<summary>
			'''NUM LOCK key
			'''</summary>
			NUMLOCK = &H90
			'''<summary>
			'''SCROLL LOCK key
			'''</summary>
			SCROLL = &H91
			'''<summary>
			'''Left SHIFT key
			'''</summary>
			LSHIFT = &HA0
			'''<summary>
			'''Right SHIFT key
			'''</summary>
			RSHIFT = &HA1
			'''<summary>
			'''Left CONTROL key
			'''</summary>
			LCONTROL = &HA2
			'''<summary>
			'''Right CONTROL key
			'''</summary>
			RCONTROL = &HA3
			'''<summary>
			'''Left MENU key
			'''</summary>
			LMENU = &HA4
			'''<summary>
			'''Right MENU key
			'''</summary>
			RMENU = &HA5
			'''<summary>
			'''Windows 2000/XP: Browser Back key
			'''</summary>
			BROWSER_BACK = &HA6
			'''<summary>
			'''Windows 2000/XP: Browser Forward key
			'''</summary>
			BROWSER_FORWARD = &HA7
			'''<summary>
			'''Windows 2000/XP: Browser Refresh key
			'''</summary>
			BROWSER_REFRESH = &HA8
			'''<summary>
			'''Windows 2000/XP: Browser Stop key
			'''</summary>
			BROWSER_STOP = &HA9
			'''<summary>
			'''Windows 2000/XP: Browser Search key
			'''</summary>
			BROWSER_SEARCH = &HAA
			'''<summary>
			'''Windows 2000/XP: Browser Favorites key
			'''</summary>
			BROWSER_FAVORITES = &HAB
			'''<summary>
			'''Windows 2000/XP: Browser Start And Home key
			'''</summary>
			BROWSER_HOME = &HAC
			'''<summary>
			'''Windows 2000/XP: Volume Mute key
			'''</summary>
			VOLUME_MUTE = &HAD
			'''<summary>
			'''Windows 2000/XP: Volume Down key
			'''</summary>
			VOLUME_DOWN = &HAE
			'''<summary>
			'''Windows 2000/XP: Volume Up key
			'''</summary>
			VOLUME_UP = &HAF
			'''<summary>
			'''Windows 2000/XP: Next Track key
			'''</summary>
			MEDIA_NEXT_TRACK = &HB0
			'''<summary>
			'''Windows 2000/XP: Previous Track key
			'''</summary>
			MEDIA_PREV_TRACK = &HB1
			'''<summary>
			'''Windows 2000/XP: Stop Media key
			'''</summary>
			MEDIA_STOP = &HB2
			'''<summary>
			'''Windows 2000/XP: Play/Pause Media key
			'''</summary>
			MEDIA_PLAY_PAUSE = &HB3
			'''<summary>
			'''Windows 2000/XP: Start Mail key
			'''</summary>
			LAUNCH_MAIL = &HB4
			'''<summary>
			'''Windows 2000/XP: Select Case Media key
			'''</summary>
			LAUNCH_MEDIA_SELECT = &HB5
			'''<summary>
			'''Windows 2000/XP: Start Application 1 key
			'''</summary>
			LAUNCH_APP1 = &HB6
			'''<summary>
			'''Windows 2000/XP: Start Application 2 key
			'''</summary>
			LAUNCH_APP2 = &HB7
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_1 = &HBA
			'''<summary>
			'''Windows 2000/XP: For any country/region the '+' key
			'''</summary>
			OEM_PLUS = &HBB
			'''<summary>
			'''Windows 2000/XP: For any country/region the '' key
			'''</summary>
			OEM_COMMA = &HBC
			'''<summary>
			'''Windows 2000/XP: For any country/region the '-' key
			'''</summary>
			OEM_MINUS = &HBD
			'''<summary>
			'''Windows 2000/XP: For any country/region the '.' key
			'''</summary>
			OEM_PERIOD = &HBE
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_2 = &HBF
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_3 = &HC0
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_4 = &HDB
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_5 = &HDC
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_6 = &HDD
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_7 = &HDE
			'''<summary>
			'''Used for miscellaneous characters; it can vary by keyboard.
			'''</summary>
			OEM_8 = &HDF
			'''<summary>
			'''Windows 2000/XP: Either the angle bracket key Or the backslash key On the RT 102-key keyboard
			'''</summary>
			OEM_102 = &HE2
			'''<summary>
			'''Windows 95/98/Me Windows NT 4.0 Windows 2000/XP: IME PROCESS key
			'''</summary>
			PROCESSKEY = &HE5
			'''<summary>
			'''Windows 2000/XP: Used to pass Unicode characters as if they were keystrokes. The VK_PACKET key Is the low word of a 32-bit Virtual Key value used for non-keyboard input methods. For more information see Remark in KEYBDINPUT SendInput WM_KEYDOWN And WM_KEYUP
			'''</summary>
			PACKET = &HE7
			'''<summary>
			'''Attn key
			'''</summary>
			ATTN = &HF6
			'''<summary>
			'''CrSel key
			'''</summary>
			CRSEL = &HF7
			'''<summary>
			'''ExSel key
			'''</summary>
			EXSEL = &HF8
			'''<summary>
			'''Erase EOF key
			'''</summary>
			EREOF = &HF9
			'''<summary>
			'''Play key
			'''</summary>
			PLAY = &HFA
			'''<summary>
			'''Zoom key
			'''</summary>
			ZOOM = &HFB
			'''<summary>
			'''Reserved
			'''</summary>
			NONAME = &HFC
			'''<summary>
			'''PA1 key
			'''</summary>
			PA1 = &HFD
			'''<summary>
			'''Clear key
			'''</summary>
			OEM_CLEAR = &HFE
		End Enum
		Private Delegate Function KeyboardHookDelegate(
	  ByVal Code As Int32,
	  ByVal wParam As Int32, ByRef lParam As KBDLLHOOKSTRUCT) _
				   As Int32

		Private Declare Function UnhookWindowsHookEx Lib "user32" _
	(ByVal hHook As Int32) As Int32

		Private Declare Function SetWindowsHookEx Lib "user32" _
	Alias "SetWindowsHookExA" (ByVal idHook As Int32,
	ByVal lpfn As KeyboardHookDelegate, ByVal hmod As Int32,
	ByVal dwThreadId As Int32) As Int32

		Private Declare Function GetAsyncKeyState Lib "user32" _
	(ByVal vKey As Int32) As Int32

		Private Declare Function CallNextHookEx Lib "user32" _
	(ByVal hHook As Int32,
	ByVal nCode As Int32,
	ByVal wParam As Int32,
	ByVal lParam As KBDLLHOOKSTRUCT) As Int32
		''' <summary>
		''' The set of valid MapTypes used in MapVirtualKey
		''' </summary>
		Public Enum MapVirtualKeyMapTypes As UInt32
			''' <summary>
			''' uCode Is a virtual-key code And Is translated into a scan code.
			''' If it Is a virtual-key code that does Not distinguish between left- And
			''' right-hand keys the left-hand scan code Is returned.
			''' If there Is no translation the function returns 0.
			''' </summary>
			MAPVK_VK_TO_VSC = &H0

			''' <summary>
			''' uCode Is a scan code And Is translated into a virtual-key code that
			''' does Not distinguish between left- And right-hand keys. If there Is no
			''' translation the function returns 0.
			''' </summary>
			MAPVK_VSC_TO_VK = &H1

			''' <summary>
			''' uCode Is a virtual-key code And Is translated into an unshifted
			''' character value in the low-order word of the return value. Dead keys (diacritics)
			''' are indicated by setting the top bit of the return value. If there Is no
			''' translation the function returns 0.
			''' </summary>
			MAPVK_VK_TO_CHAR = &H2

			''' <summary>
			''' Windows NT/2000/XP: uCode Is a scan code And Is translated into a
			''' virtual-key code that distinguishes between left- And right-hand keys. If
			''' there Is no translation the function returns 0.
			''' </summary>
			MAPVK_VSC_TO_VK_EX = &H3

			''' <summary>
			''' Not currently documented
			''' </summary>
			MAPVK_VK_TO_VSC_EX = &H4
		End Enum
		<DllImport("user32.dll")>
		Private Shared Function MapVirtualKeyEx(ByVal uCode As UInteger, ByVal uMapType As MapVirtualKeyMapTypes, ByVal dwhkl As IntPtr) As UInt32
		End Function

		Public Structure KBDLLHOOKSTRUCT
			Public vkCode As VK_ENUM
			Public scanCode As Int32
			Public flags As KBFLAGS
			Public time As Int32
			Public dwExtraInfo As Int32
		End Structure

		' Low-Level Keyboard Constants
		''' <summary>
		''' The wParam and lParam parameters contain information about a keystroke message.
		''' </summary>
		Public Const HC_ACTION As Int32 = 0
		Public Enum KBFLAGS As Int32
			''' <summary>
			''' Test the extended-key flag. 
			''' </summary>
			LLKHF_EXTENDED = &H1
			''' <summary>
			''' Test the event-injected (from any process) flag. 
			''' </summary>
			LLKHF_INJECTED = &H10
			''' <summary>
			''' Test the event-injected (from a process running at lower integrity level) flag. 
			''' </summary>
			LLKHF_LOWER_IL_INJECTED = &H2
			''' <summary>
			''' Test the context code. The context code. The value is 1 if the ALT key is pressed; otherwise, it is 0.
			''' </summary>
			LLKHF_ALTDOWN = &H20
			''' <summary>
			''' Test the transition-state flag. The transition state. The value is 0 if the key is pressed and 1 if it is being released.
			''' </summary>
			LLKHF_UP = &H80
		End Enum

		' Virtual Keys
		'Use ConsoleKey

		Private Const WH_KEYBOARD_LL As Int32 = &H13
		''' <summary>
		''' Converts char to virtual key
		''' </summary>
		''' <param name="ch">Char to be converted</param>
		''' <returns>VK_ENUM</returns>
		<DllImport("user32.dll", CharSet:=CharSet.Ansi)>
		Private Shared Function VkKeyScan(ByVal ch As Char) As VK_ENUM
		End Function
		<DllImport("user32.dll")>
		Private Shared Function GetFocus() As IntPtr
		End Function
#End Region
		Public Property PollingTime As Double = 100
		Dim DI As New DirectInput.DirectInput()
		Dim WithEvents kbd As DirectInput.Keyboard = Nothing
		Dim WithEvents kbdstate As DirectInput.KeyboardState
		Dim WithEvents tmr As New Timers.Timer(PollingTime)
		Public Event KeyPress(ByVal sender As Object, ByVal e As DirectInput.KeyboardUpdate)
		Public Event KeyDown(ByVal sender As Object, ByVal e As DirectInput.KeyboardUpdate)
		Public Event KeyUp(ByVal sender As Object, ByVal e As DirectInput.KeyboardUpdate)
		Public Sub New()
			MyBase.New(InputDeviceType.Keyboard)
			If Me.UseWin32API Then

			Else
				'use DirectX Input
				For Each k As DirectInput.DeviceInstance In DI.GetDevices(DirectInput.DeviceClass.Keyboard, DirectInput.DeviceEnumerationFlags.AllDevices)
					' just make sure we have one keyboard available
					If k.Type = DirectInput.DeviceType.Keyboard And Not k.InstanceGuid = Guid.Empty Then
						kbd = New DirectInput.Keyboard(DI)
						kbd.SetCooperativeLevel(Marshal.GetHINSTANCE(Reflection.[Assembly].GetExecutingAssembly.GetModules()(0)).ToInt32(), DirectInput.CooperativeLevel.NonExclusive Or DirectInput.CooperativeLevel.Background)
						Exit For
					End If
				Next
				If IsNothing(kbd) Then
					Throw New Exception("Error: No keyboard available.")
				End If
				kbd.Acquire()
				tmr.Start()
			End If
		End Sub
		Private Function ConvertDIKeyToVKKey(ByVal k As DirectInput.Key) As VK_ENUM
			Select Case k
				Case DirectInput.Key.A
					Return VK_ENUM.KEY_A
				Case DirectInput.Key.AbntC1
					Return VK_ENUM.LAUNCH_APP1
				Case DirectInput.Key.AbntC2
					Return VK_ENUM.LAUNCH_APP2
				Case DirectInput.Key.Add
					Return VK_ENUM.ADD
				Case DirectInput.Key.Apostrophe
					Return VK_ENUM.OEM_7
				Case DirectInput.Key.Applications
					Return VK_ENUM.APPS
				Case DirectInput.Key.AT
				Case DirectInput.Key.AX
				Case DirectInput.Key.B
					Return VK_ENUM.KEY_B
				Case DirectInput.Key.Backslash
					Return VK_ENUM.OEM_5
				Case DirectInput.Key.C
					Return VK_ENUM.KEY_C
				Case DirectInput.Key.Calculator
				Case DirectInput.Key.Capital
					Return VK_ENUM.CAPITAL
				Case DirectInput.Key.Colon
					Return VK_ENUM.OEM_1
				Case DirectInput.Key.Comma
					Return VkKeyScan(",")
				Case DirectInput.Key.Convert
					Return VK_ENUM.CONVERT
				Case DirectInput.Key.D
					Return VK_ENUM.KEY_D
				Case DirectInput.Key.D0
					Return VK_ENUM.KEY_0
				Case DirectInput.Key.D1
					Return VK_ENUM.KEY_1
				Case DirectInput.Key.D2
					Return VK_ENUM.KEY_2
				Case DirectInput.Key.D3
					Return VK_ENUM.KEY_3
				Case DirectInput.Key.D4
					Return VK_ENUM.KEY_4
				Case DirectInput.Key.D5
					Return VK_ENUM.KEY_5
				Case DirectInput.Key.D6
					Return VK_ENUM.KEY_6
				Case DirectInput.Key.D7
					Return VK_ENUM.KEY_7
				Case DirectInput.Key.D8
					Return VK_ENUM.KEY_8
				Case DirectInput.Key.D9
					Return VK_ENUM.KEY_9
				Case DirectInput.Key.Decimal
					Return VK_ENUM.DECIMAL_KEY
				Case DirectInput.Key.Delete
					Return VK_ENUM.DECIMAL_KEY
				Case DirectInput.Key.Divide
					Return VK_ENUM.DIVIDE
				Case DirectInput.Key.Down
					Return VK_ENUM.DOWN
				Case DirectInput.Key.E
					Return VK_ENUM.KEY_E
				Case DirectInput.Key.End
					Return VK_ENUM.KEY_E
				Case DirectInput.Key.Equals
					Return VkKeyScan("=")
				Case DirectInput.Key.Escape
					Return VK_ENUM.ESCAPE
				Case DirectInput.Key.F
					Return VK_ENUM.F
				Case DirectInput.Key.F1
					Return VK_ENUM.F2
				Case DirectInput.Key.F2
					Return VK_ENUM.F2
				Case DirectInput.Key.F3
					Return VK_ENUM.F3
				Case DirectInput.Key.F4
					Return VK_ENUM.F4
				Case DirectInput.Key.F5
					Return VK_ENUM.F5
				Case DirectInput.Key.F6
					Return VK_ENUM.F6
				Case DirectInput.Key.F7
					Return VK_ENUM.F7
				Case DirectInput.Key.F8
					Return VK_ENUM.F8
				Case DirectInput.Key.F9
					Return VK_ENUM.F9
				Case DirectInput.Key.F10
					Return VK_ENUM.F10
				Case DirectInput.Key.F11
					Return VK_ENUM.F11
				Case DirectInput.Key.F12
					Return VK_ENUM.F12
				Case DirectInput.Key.F13
					Return VK_ENUM.F13
				Case DirectInput.Key.F14
					Return VK_ENUM.F14
				Case DirectInput.Key.F15
					Return VK_ENUM.F15
				Case DirectInput.Key.G
					Return VK_ENUM.KEY_G
				Case DirectInput.Key.Grave
				Case DirectInput.Key.H
					Return VK_ENUM.KEY_H
				Case DirectInput.Key.Home
					Return VK_ENUM.HOME
				Case DirectInput.Key.I
					Return VK_ENUM.KEY_I
				Case DirectInput.Key.Insert
					Return VK_ENUM.INSERT
				Case DirectInput.Key.J
					Return VK_ENUM.KEY_J
				Case DirectInput.Key.K
					Return VK_ENUM.KEY_K
				Case DirectInput.Key.Kana
				Case DirectInput.Key.Kanji
				Case DirectInput.Key.L
					Return VK_ENUM.KEY_L
				Case DirectInput.Key.Left
					Return VK_ENUM.LEFT
				Case DirectInput.Key.LeftAlt
					Return VK_ENUM.LMENU
				Case DirectInput.Key.LeftBracket
					Return VK_ENUM.OEM_4
				Case DirectInput.Key.LeftControl
					Return VK_ENUM.LCONTROL
				Case DirectInput.Key.LeftShift
					Return VK_ENUM.LSHIFT
				Case DirectInput.Key.LeftWindowsKey
					Return VK_ENUM.LWIN
				Case DirectInput.Key.M
					Return VK_ENUM.KEY_M
				Case DirectInput.Key.Mail
					Return VK_ENUM.LAUNCH_MAIL
				Case DirectInput.Key.MediaSelect
					Return VK_ENUM.LAUNCH_MEDIA_SELECT
				Case DirectInput.Key.MediaStop
					Return VK_ENUM.MEDIA_STOP
				Case DirectInput.Key.Minus
					Return VK_ENUM.OEM_MINUS
				Case DirectInput.Key.Multiply
					Return VK_ENUM.MULTIPLY
				Case DirectInput.Key.Mute
					Return VK_ENUM.VOLUME_MUTE
				Case DirectInput.Key.MyComputer
				Case DirectInput.Key.N
					Return VK_ENUM.KEY_N
				Case DirectInput.Key.NextTrack
					Return VK_ENUM.MEDIA_NEXT_TRACK
				Case DirectInput.Key.NoConvert
					Return VK_ENUM.NONCONVERT
				Case DirectInput.Key.NumberLock
					Return VK_ENUM.NUMLOCK
				Case DirectInput.Key.NumberPad0
					Return VK_ENUM.NUMPAD0
				Case DirectInput.Key.NumberPad1
					Return VK_ENUM.NUMPAD1
				Case DirectInput.Key.NumberPad2
					Return VK_ENUM.NUMPAD2
				Case DirectInput.Key.NumberPad3
					Return VK_ENUM.NUMPAD3
				Case DirectInput.Key.NumberPad4
					Return VK_ENUM.NUMPAD4
				Case DirectInput.Key.NumberPad5
					Return VK_ENUM.NUMPAD5
				Case DirectInput.Key.NumberPad6
					Return VK_ENUM.NUMPAD6
				Case DirectInput.Key.NumberPad7
					Return VK_ENUM.NUMPAD7
				Case DirectInput.Key.NumberPad8
					Return VK_ENUM.NUMPAD8
				Case DirectInput.Key.NumberPad9
					Return VK_ENUM.NUMPAD9
				Case DirectInput.Key.NumberPadComma
					Return VK_ENUM.OEM_COMMA
				Case DirectInput.Key.NumberPadEnter
					Return VK_ENUM.ENTER
				Case DirectInput.Key.NumberPadEquals
					Return VkKeyScan("=")
				Case DirectInput.Key.O
					Return VK_ENUM.KEY_O
				Case DirectInput.Key.Oem102
					Return VK_ENUM.OEM_102
				Case DirectInput.Key.P
					Return VK_ENUM.KEY_P
				Case DirectInput.Key.PageDown
					Return VK_ENUM.PAGE_DOWN
				Case DirectInput.Key.PageUp
					Return VK_ENUM.PAGE_UP
				Case DirectInput.Key.Pause
					Return VK_ENUM.PAUSE
				Case DirectInput.Key.Period
					Return VK_ENUM.OEM_PERIOD
				Case DirectInput.Key.PlayPause
					Return VK_ENUM.MEDIA_PLAY_PAUSE
				Case DirectInput.Key.Power
					Return VK_ENUM.SLEEP
				Case DirectInput.Key.PreviousTrack
					Return VK_ENUM.MEDIA_PREV_TRACK
				Case DirectInput.Key.PrintScreen
					Return VK_ENUM.PRINT
				Case DirectInput.Key.Q
					Return VK_ENUM.KEY_Q
				Case DirectInput.Key.R
					Return VK_ENUM.KEY_R
				Case DirectInput.Key.Return
					Return VK_ENUM.ENTER
				Case DirectInput.Key.Right
					Return VK_ENUM.RIGHT
				Case DirectInput.Key.RightAlt
					Return VK_ENUM.RMENU
				Case DirectInput.Key.RightBracket
					Return VK_ENUM.OEM_6
				Case DirectInput.Key.RightControl
					Return VK_ENUM.RCONTROL
				Case DirectInput.Key.RightShift
					Return VK_ENUM.RSHIFT
				Case DirectInput.Key.RightWindowsKey
					Return VK_ENUM.RWIN
				Case DirectInput.Key.S
					Return VK_ENUM.KEY_S
				Case DirectInput.Key.ScrollLock
					Return VK_ENUM.SCROLL
				Case DirectInput.Key.Semicolon
					Return VK_ENUM.OEM_1
				Case DirectInput.Key.Slash
					Return VK_ENUM.OEM_2
				Case DirectInput.Key.Sleep
					Return VK_ENUM.SLEEP
				Case DirectInput.Key.Space
					Return VK_ENUM.SPACE
				Case DirectInput.Key.Stop
					Return VK_ENUM.MEDIA_STOP
				Case DirectInput.Key.Subtract
					Return VK_ENUM.SUBTRACT
				Case DirectInput.Key.T
					Return VK_ENUM.KEY_T
				Case DirectInput.Key.Tab
					Return VK_ENUM.TAB
				Case DirectInput.Key.U
					Return VK_ENUM.KEY_U
				Case DirectInput.Key.Underline
					Return VK_ENUM.OEM_MINUS
				Case DirectInput.Key.Unknown
					Return VK_ENUM.NONAME
				Case DirectInput.Key.Unlabeled
					Return VK_ENUM.NONAME
				Case DirectInput.Key.Up
					Return VK_ENUM.UP
				Case DirectInput.Key.V
					Return VK_ENUM.KEY_V
				Case DirectInput.Key.VolumeDown
					Return VK_ENUM.VOLUME_DOWN
				Case DirectInput.Key.VolumeUp
					Return VK_ENUM.VOLUME_UP
				Case DirectInput.Key.W
					Return VK_ENUM.KEY_W
				Case DirectInput.Key.Wake
				Case DirectInput.Key.WebBack
					Return VK_ENUM.BROWSER_BACK
				Case DirectInput.Key.WebFavorites
					Return VK_ENUM.BROWSER_FAVORITES
				Case DirectInput.Key.WebForward
					Return VK_ENUM.BROWSER_FORWARD
				Case DirectInput.Key.WebHome
					Return VK_ENUM.BROWSER_HOME
				Case DirectInput.Key.WebRefresh
					Return VK_ENUM.BROWSER_REFRESH
				Case DirectInput.Key.WebSearch
					Return VK_ENUM.BROWSER_SEARCH
				Case DirectInput.Key.WebStop
					Return VK_ENUM.BROWSER_STOP
				Case DirectInput.Key.X
					Return VK_ENUM.KEY_X
				Case DirectInput.Key.Y
					Return VK_ENUM.KEY_Y
				Case DirectInput.Key.Yen
				Case DirectInput.Key.Z
					Return VK_ENUM.KEY_Z
				Case Else
					Return -1

			End Select
		End Function
		''' <summary>
		''' 
		''' </summary>
		''' <returns></returns>
		Public Function GetPressedKeys() As Byte()
			Dim ret(256) As Byte
			If Me.UseWin32API Then
				If GetKeyboardState(ret) > 0 Then
					Return ret
				End If
			Else
				'use DirectX Input
				Dim keys As List(Of DirectInput.Key) = kbd.GetCurrentState.PressedKeys()
				For Each key As DirectInput.Key In keys
					ret.add()
				Next
				Return kbd.GetCurrentState.PressedKeys()(0).
			End If
		End Function

		Private Sub tmr_Elapsed(sender As Object, e As ElapsedEventArgs) Handles tmr.Elapsed
			If Me.UseWin32API Then

			Else
				'use DirectX Input
				If kbd.GetCurrentState.PressedKeys().Count > 0 Then
					RaiseEvent KeyPress(sender, kbd.GetBufferedData().First)
				End If
			End If
		End Sub
	End Class
End Namespace
