﻿Imports libEmu.IO.Devices
''' <summary>
''' Types of input devices
''' </summary>
Public Enum InputDeviceType
    Keyboard
    Mouse
    Bluetooth
    Joystick
	Gamepad
	HID
	RawDevice
    UnknownDevice
End Enum
Namespace IO.Input
    Public Class InputDevice
        Inherits Device
        ''' <summary>
        ''' The type of input device.
        ''' </summary>
        ''' <see cref="InputDeviceType"/>
        ''' <returns>The device type, if unknown/invalid will return UnknownDevice</returns>
        Public ReadOnly Property InputType As InputDeviceType = InputDeviceType.UnknownDevice
		''' <summary>
		''' Use the Win32API for mouse/keyboard input rather than DirectX.
		''' By default, it is enabled.
		''' </summary>
		''' <returns>True or False</returns>
		Public Property UseWin32API As Boolean = True
		''' <summary>
		''' Use raw input for mouse input rather than DirectX.
		''' </summary>
		''' <remarks>Disables the use of <see cref="UseWin32API"/></remarks>
		''' <returns>True or False</returns>
		Public Property UseRawInput As Boolean = False

		''' <summary>
		''' 
		''' </summary>
		''' <param name="devType">The type of device</param>
		Public Sub New(ByVal devType As InputDeviceType)
            MyBase.New(DeviceType.Input, {&H0, &H0})
            Me.InputType = devType
        End Sub

        ''' <summary>
        ''' Gets the address of the device
        ''' </summary>
        ''' <returns>Address of device</returns>
        Public Function GetAddress() As Int64
            Return Nothing
        End Function
        ''' <summary>
        ''' Read a value at the specified address
        ''' </summary>
        ''' <param name="address">Address of where to read the value</param>
        ''' <returns>Value at the specified address</returns>
        Public Overridable Function Read(ByVal address As Int64) As Object
            Return Nothing
        End Function
        ''' <summary>
        ''' Write a value at the specified address
        ''' </summary>
        ''' <param name="address">Address where to write the value</param>
        ''' <param name="value">The value to write</param>
        ''' <returns></returns>
        Public Overridable Function Write(ByVal address As Int64, ByVal value As Object) As Boolean
            Return True
        End Function
    End Class
End Namespace
