﻿Imports System.Net
Imports System.Net.NetworkInformation
Imports System.Net.Sockets

Public Class NetworkDevice
	Inherits IO.Devices.Device

	Public Property MAC As PhysicalAddress
	Public Property IPv4 As IPAddress
		Get
			Dim uniCast As UnicastIPAddressInformationCollection = iface.GetIPProperties().UnicastAddresses

			If uniCast.Count > 0 Then
				For Each uni As UnicastIPAddressInformation In uniCast
					If uni.PrefixOrigin = PrefixOrigin.Dhcp Then
						' This indicates the assigned IP address by the router's DHCP pool
						Return uni.IPv4Mask
					End If
				Next
			End If
			Return IPAddress.None
		End Get
		Set(value As IPAddress)

		End Set
	End Property
	Public Property IPv6 As IPAddress
	Public Property DNS As IPAddressCollection
	Public Property WINS As IPAddressCollection
	Public Property Gateway As IPAddress
	Public Property Subnet As IPAddress
	Public Property HostName As String
	Private iface As NetworkInterface

	Private Function GetLocalIPv4(ByVal _type As NetworkInterfaceType) As String()
		Dim ipAddrList As List(Of String) = New List(Of String)()

		For Each item As NetworkInterface In NetworkInterface.GetAllNetworkInterfaces()

			If item.NetworkInterfaceType = _type AndAlso item.OperationalStatus = OperationalStatus.Up Then

				For Each ip As UnicastIPAddressInformation In item.GetIPProperties().UnicastAddresses

					If ip.Address.AddressFamily = AddressFamily.InterNetwork Then
						ipAddrList.Add(ip.Address.ToString())
					End If
				Next
			End If
		Next

		Return ipAddrList.ToArray()
	End Function

	Private Function GetNicIPv4() As IPAddress
		Dim uniCast As UnicastIPAddressInformationCollection = iface.GetIPProperties().UnicastAddresses

		If uniCast.Count > 0 Then
			For Each uni As UnicastIPAddressInformation In uniCast
				If uni.PrefixOrigin = PrefixOrigin.Dhcp Then
					' This indicates the assigned IP address by the router's DHCP pool
					Return uni.Address
				End If
			Next
		End If
		Return IPAddress.None
	End Function
	Private Function GetNicIPv6() As IPAddress
		Dim uniCast As UnicastIPAddressInformationCollection = iface.GetIPProperties().UnicastAddresses

		If uniCast.Count > 0 Then
			For Each uni As UnicastIPAddressInformation In uniCast
				If uni.PrefixOrigin = PrefixOrigin.RouterAdvertisement And uni.SuffixOrigin = SuffixOrigin.LinkLayerAddress Then
					' This indicates the assigned IP address by the router
					Return uni.Address
				End If
			Next
		End If
		Return IPAddress.None
	End Function
	Private Function GetNicSubnetMask() As IPAddress
		Dim uniCast As UnicastIPAddressInformationCollection = iface.GetIPProperties().UnicastAddresses

		If uniCast.Count > 0 Then
			For Each uni As UnicastIPAddressInformation In uniCast
				If uni.PrefixOrigin = PrefixOrigin.Dhcp Then
					' This indicates the assigned IP address by the router's DHCP pool
					Return uni.IPv4Mask
				End If
			Next
		End If
		Return IPAddress.None
	End Function
	Public Sub New(ByVal nic As NetworkInterface)
		MyBase.New(IO.Devices.DeviceType.Network)
		iface = nic
		Me.MAC = nic.GetPhysicalAddress()
		Me.HostName = Net.Dns.GetHostName()
		Me.IPv4 = New IPAddress(System.Text.Encoding.UTF8.GetBytes(GetLocalIPv4(NetworkInterfaceType.Ethernet).FirstOrDefault()))
		Me.IPv6 = Me.IPv4.MapToIPv6

	End Sub
	Public Sub New(Optional ByVal hardwareMac As PhysicalAddress = Nothing)
		MyBase.New(IO.Devices.DeviceType.Network)

	End Sub

End Class
