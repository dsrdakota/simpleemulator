﻿Imports System.Net.NetworkInformation

Public Class Network


	Public Sub New()


	End Sub
	Public Function GetAllDevices() As List(Of NetworkDevice)
		Dim ret As New List(Of NetworkDevice)
		Dim pc As IPGlobalProperties = IPGlobalProperties.GetIPGlobalProperties()
		Dim nics As NetworkInterface() = NetworkInterface.GetAllNetworkInterfaces()
		If nics.Count < 1 Then Return ret
		For i As Int32 = 0 To nics.Count
			ret.Add(New NetworkDevice(nics(i).GetPhysicalAddress()))
		Next
		Return ret
	End Function
End Class
