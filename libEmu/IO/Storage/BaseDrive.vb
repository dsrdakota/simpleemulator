﻿Imports libEmu.IO.Devices

Namespace IO.Storage
    Public Class DiskGeometry
#Region "LBA and CHS"
        '    CHS tuples can be mapped To LBA address With the following formula:[6][7]

        '    LBA = (C × HPC + H) × SPT + (S − 1)

        'where

        '    C, H And S are the cylinder number, the head number, And the sector number
        '    LBA Is the logical block address
        '    HPC Is the maximum number of heads per cylinder (reported by disk drive, typically 16 for 28-bit LBA)
        '    SPT Is the maximum number of sectors per track (reported by disk drive, typically 63 for 28-bit LBA)

        'LBA addresses can be mapped To CHS tuples With the following formula ("mod" Is the modulo operation,
        'i.e. the remainder, And "÷" Is Integer division, i.e. the quotient Of the division where any fractional
        'part Is discarded):

        '    C = LBA ÷ (HPC × SPT)
        '    H = (LBA ÷ SPT) Mod HPC
        '    S = (LBA Mod SPT) + 1

        'According to the ATA specifications, "If the content of words (61:60) is greater than or equal to 16,514,064, 
        'then the content of word 1 [the number of logical cylinders] shall be equal to 16,383."
        '    [1] Therefore, for LBA 16450559, an ATA drive may actually respond with the CHS tuple (16319, 15, 63),
        '    And the number of cylinders in this scheme must be much larger than 1024 allowed by INT 13h.[a] 
#End Region

        Public Property Cylinders As Int64 = 0
        Public Property Heads As Int64 = 0
        'Usually 16
        Public Property HeadsPerCylinder As Int64 = 16
        Public Property SectorsPerTrack As Int64 = 63
        Public Property Sectors As Int64 = 0
        Public Property BytesPerSector As Int64 = 512
        Public Property LBA As Int64 = 0
        Public Sub New(ByVal S As Long, Optional ByVal H As Int64 = 255, Optional ByVal SPT As Int64 = 63)
            Sectors = S
            Heads = H
            SectorsPerTrack = SPT
            Cylinders = Math.Floor(Sectors / Heads / SectorsPerTrack)
            CalculateLBA()
        End Sub
        Private Function ValidateGeometry() As Boolean
            'Heads per cylinder usually 16 npt specified
            Return Cylinders > 0 And Heads > 0 And Sectors > 0 And SectorsPerTrack > 0 And SectorsPerTrack < 64 And HeadsPerCylinder > 0
        End Function
        Public Function CalculateLBA() As Long
            Dim ret As Long
            If Not ValidateGeometry() Then
                Throw New Exception("Invalid disk geometry.")
                Return 0
            End If
            LBA = (Cylinders * HeadsPerCylinder + Heads) * SectorsPerTrack + (Sectors - 1)
            Return LBA
        End Function
    End Class
    Public Class BaseDrive
        Inherits Device
        Public Property Size As Int64
        Public Property Geometry As DiskGeometry
        Public Sub New()
            MyBase.New(DeviceType.Storage)
        End Sub
    End Class
End Namespace
