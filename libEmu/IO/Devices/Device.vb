﻿Namespace IO.Devices

    Public Enum DeviceType
        Input
        Storage
        Display
        USB
        Serial
		Network
	End Enum
    Public Class Device

        Public ReadOnly Property Manufacturer As String = ""
        Public ReadOnly Property ModelName As String = ""
        Public ReadOnly Property HardwareId As Int64()
        Public ReadOnly Property DeviceId As Int64
        Public ReadOnly Property VendorId As Int64
        Public ReadOnly Property DeviceType As DeviceType
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="deviceType"></param>
        ''' <param name="hardwareId"></param>
        Public Sub New(ByVal deviceType As DeviceType, ByVal hardwareId As Int64())
            If Not IsNothing(hardwareId) And hardwareId.Count > 1 Then
                VendorId = hardwareId(0)
                DeviceId = hardwareId(1)

            End If
            deviceType = deviceType
        End Sub
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="deviceType"></param>
        Public Sub New(ByVal deviceType As DeviceType)
            Me.New(deviceType, {&H0, &H0})
        End Sub
    End Class
End Namespace