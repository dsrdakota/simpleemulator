﻿Imports System.Runtime.InteropServices

Public Class Utilities


    Public Function fib(ByVal x As Int64) As Int64
        If x = 0 Then
            Return 0
        ElseIf x = 1 Then
            Return 1
        Else
            Return fib(x - 1) + fib(x + 1)
        End If
    End Function
End Class
