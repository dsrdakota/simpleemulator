﻿Imports System.IO
Imports System.Net.NetworkInformation
Imports System.Text

Public Class frmMain
	Public Shared Sub DisplayUnicastAddresses()

		Dim w As StreamWriter = FileIO.FileSystem.OpenTextFileWriter("output.txt", False)
		w.WriteLine("Unicast Addresses")
		Dim adapters As NetworkInterface() = NetworkInterface.GetAllNetworkInterfaces()

		For Each adapter As NetworkInterface In adapters
			Dim adapterProperties As IPInterfaceProperties = adapter.GetIPProperties()
			Dim uniCast As UnicastIPAddressInformationCollection = adapterProperties.UnicastAddresses

			If uniCast.Count > 0 Then
				w.WriteLine(adapter.Description)
				Dim lifeTimeFormat As String = "dddd, MMMM dd, yyyy  hh:mm:ss tt"

				For Each uni As UnicastIPAddressInformation In uniCast
					Dim [when] As Date
					w.WriteLine("  Unicast Address ......................... : {0}", uni.Address)
					w.WriteLine("  Unicast Address v6 ....................... : {0}", uni.Address.MapToIPv6())
					w.WriteLine("  IPv4Mask        ......................... : {0}", uni.IPv4Mask)
					w.WriteLine("     Prefix Origin ........................ : {0}", uni.PrefixOrigin)
					w.WriteLine("     Suffix Origin ........................ : {0}", uni.SuffixOrigin)
					w.WriteLine("     Duplicate Address Detection .......... : {0}", uni.DuplicateAddressDetectionState)
					[when] = DateTime.UtcNow + TimeSpan.FromSeconds(uni.AddressValidLifetime)
					[when] = [when].ToLocalTime()
					w.WriteLine("     Valid Life Time ...................... : {0}", [when].ToString(lifeTimeFormat, System.Globalization.CultureInfo.CurrentCulture))
					[when] = DateTime.UtcNow + TimeSpan.FromSeconds(uni.AddressPreferredLifetime)
					[when] = [when].ToLocalTime()
					w.WriteLine("     Preferred life time .................. : {0}", [when].ToString(lifeTimeFormat, System.Globalization.CultureInfo.CurrentCulture))
					[when] = DateTime.UtcNow + TimeSpan.FromSeconds(uni.DhcpLeaseLifetime)
					[when] = [when].ToLocalTime()
					w.WriteLine("     DHCP Leased Life Time ................ : {0}", [when].ToString(lifeTimeFormat, System.Globalization.CultureInfo.CurrentCulture))
				Next

				w.WriteLine("")
			End If
		Next

	End Sub
	Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
		DisplayUnicastAddresses()
	End Sub
End Class
